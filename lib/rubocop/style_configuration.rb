require 'rubocop/config_loader'

module RuboCop
  class ConfigLoader
    class << self
      def configuration_file_for(_target_dir)
        File.join(File.expand_path(__FILE__), '..', '..', '..', 'config', 'rubocop.yml')
      end
    end
  end
end
