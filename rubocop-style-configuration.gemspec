# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rubocop/style_configuration/version'

Gem::Specification.new do |spec|
  spec.name          = 'rubocop-style-configuration'
  spec.version       = RuboCop::StyleConfiguration::VERSION
  spec.authors       = ['Michał Połtyn', 'Ed King']
  spec.email         = %w(no-reply@cloudcredo.com)
  spec.summary       = 'Rubocop global config'
  spec.homepage      = 'https://bitbucket.org/ccp/rubocop-style-configuration'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.test_files    = spec.files.grep(/^spec\//)
  spec.executables   = `git ls-files -- bin/*`.split("\n").map { |f| File.basename(f) }
  spec.require_paths = %w(lib)

  spec.add_development_dependency 'bundler',      '~> 1.6'
  spec.add_development_dependency 'rake',         '~> 10.3'
  spec.add_development_dependency 'rspec',        '~> 3.0.0'
  spec.add_development_dependency 'rest_client'
  spec.add_development_dependency 'pry'

  spec.add_dependency 'rubocop'
end
